


def CheckNextFreeCell(nextCell,snake,board):
    # This funcition check if the next cell of the snake is available

    if (nextCell[0] > board[0]-1) or (nextCell[1] > board[1]-1) or (nextCell[0]<0) or (nextCell[1]<0):
        return False

    for i in snake:
        snakeValue=i
        if(snakeValue[0]==nextCell[0]) and (snakeValue[1]==nextCell[1]):
            return False


    snake.insert(0,nextCell)
    return True

def MoveSnakeDirection(direction,snake,board):
    del snake[-1]
    headSnake=snake[0]
    if direction=="U":
        nextCell=[headSnake[0],headSnake[1]-1]
        result=CheckNextFreeCell(nextCell,snake,board)
    if direction=="D":
        nextCell=[headSnake[0],headSnake[1]+1]
        result=CheckNextFreeCell(nextCell,snake,board)
    if direction=="L":
        nextCell=[headSnake[0]-1,headSnake[1]]
        result=CheckNextFreeCell(nextCell,snake,board)
    if direction=="R":
        nextCell=[headSnake[0]+1,headSnake[1]]
        result=CheckNextFreeCell(nextCell,snake,board) 
    
    if result==False:
        print (direction +" out")
    else:
        print(direction)

    return result

def NumberOfAvailableDifferentPaths(board, snake, depth):
    totalValidPaths=0
    firstSnake=snake.copy()
    
    directions=["R","D","U","L"]
    j=0
    i=0
    while j<len(directions):
        direction=directions[j]
        result=MoveSnakeDirection(direction,snake,board)

        if result==False:
            snake=firstSnake.copy()
            j+=1
            print("")
            i=0
            if j == len(directions):
                break
        else:
            if((i+1)==depth):
                totalValidPaths+=1
                j+=1
                i=0
                print("Path ok")
                print("")
            else:
                i+=1
            
    return totalValidPaths



if __name__ == "__main__":
    #board=[4,3]
    #snake=[[2,2], [3,2], [3,1], [3,0], [2,0], [1,0], [0,0]]
    #depth=2

    #board=[10, 10]
    #snake=[[0,2], [0,1], [0,0], [1,0], [1,1], [1,2]]
    #depth=5

    board=[10, 10]
    snake=[[5,5], [5,4], [4,4], [4,5]]
    depth=4

    totalValidPaths=NumberOfAvailableDifferentPaths(board,snake,depth)

    print("totalValidPaths: "+str(totalValidPaths))
